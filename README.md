# Git commit test

This project is to test the Git specific commit removal from dev and master

Initially we have pushed all the content to dev and raising a PR to main

Post that, we will cherry-pick and remove the dev commits

## Remove the key file references from Git History

1. Remove file references
git filter-branch --tree-filter 'rm -f selfsigned.key' HEAD

2. Push to origin
git push --force-with-lease origin HEAD